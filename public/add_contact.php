<?php

define('APP_DIR', __DIR__ . '/../app/');
define('APP_DIR_VALIDATE', __DIR__ . '/../app/validate/');
require_once __DIR__ . '/autoloader.php';

$request = new Request();

if ($request->isPost()) {
    $dataContact = $request->requestData();

    $phone = $request->filter($dataContact['phone']);
    $name = $request->filter($dataContact['name']);
    $surname = $request->filter($dataContact['surname']);
    $city = $request->filter($dataContact['city']);
    $street = $request->filter($dataContact['street']);
    $house = $request->filter($dataContact['house']);
    $flat = $request->filter($dataContact['flat']);

    $flag = true;

    $validateEmpty = new EmptyValidator();
    $validateLength = new LengthValidator();

}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Add phone</title>
</head>
<body>


<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
    <table>
        <tr>
            <td>Phone:</td>
            <td>
                <input type="tel" name="phone" title="phone">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($phone)) {
                        echo $validateEmpty->validate($phone);
                    }
                    if ($validateLength->validate($phone)) {
                        echo $validateLength->validate($phone);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Name:</td>
            <td>
                <input type="text" name="name" title="name">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($name)) {
                        echo $validateEmpty->validate($name);
                    }
                    if ($validateLength->validate($name)) {
                        echo $validateLength->validate($name);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Surname:</td>
            <td>
                <input type="text" name="surname" title="surname">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($surname)) {
                        echo $validateEmpty->validate($surname);
                    }
                    if ($validateLength->validate($surname)) {
                        echo $validateLength->validate($surname);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>City:</td>
            <td>
                <input type="text" name="city" title="city">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($city)) {
                        echo $validateEmpty->validate($city);
                    }
                    if ($validateLength->validate($city)) {
                        echo $validateLength->validate($city);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Street:</td>
            <td>
                <input type="text" name="street" title="street">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($street)) {
                        echo $validateEmpty->validate($street);
                    }
                    if ($validateLength->validate($street)) {
                        echo $validateLength->validate($street);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>House:</td>
            <td>
                <input type="text" name="house" title="house">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($house)) {
                        echo $validateEmpty->validate($house);
                    }
                    if ($validateLength->validate($house)) {
                        echo $validateLength->validate($house);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Flat:</td>
            <td>
                <input type="text" name="flat" title="flat">
                <?php
                if($request->isPost()) {
                    if ($validateEmpty->validate($flat)) {
                        echo $validateEmpty->validate($flat);
                    }
                    if ($validateLength->validate($flat)) {
                        echo $validateLength->validate($flat);
                    }
                }
                ?>
            </td>
        </tr>
    </table>
    <input type="submit" value="send">
</form>
</body>
</html>