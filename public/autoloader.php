<?php

class MyAutoloader
{
    public static function ClassLoader($className) {
        if (file_exists(APP_DIR . "$className.php")) {
            require_once APP_DIR . "$className.php";
            return true;
        } else {
            return false;
        }
    }
    public static function ClassLoader2($className) {
        if (file_exists(APP_DIR_VALIDATE . "$className.php")) {
            require_once APP_DIR_VALIDATE . "$className.php";
            return true;
        } else {
            return false;
        }
    }

}

spl_autoload_register('MyAutoloader::ClassLoader');
spl_autoload_register('MyAutoloader::ClassLoader2');
