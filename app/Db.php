<?php

class Db {

    protected static $instance;

    private function __construct()
    {
        $db_ms = 'mysql';
        $host = "192.168.114.129";
        $db_name = "phonebook";
        $login = "root";
        $pass = "pass@word1MINT";
        try {
            $this->db = new PDO ($db_ms . ": host=" . $host . "; db_name=" . $db_name, $login, $pass);
        }
        catch (PDOException $e){
            die("Error: " . $e->getMessage());
        }
    }

    private function __destruct()
    {
        $this->db = null;
    }

    private function __clone()
    {
    }

    static public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


}