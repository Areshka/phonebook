<?php

class Request implements RequestInterface
{
    protected $method = 'GET';
    protected $requestData;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->requestData = $_REQUEST;
    }

    public function isPost()
    {
        if ($this->method == 'POST') {
            return true;
        }
        return false;
    }

    public function isGet()
    {
        if ($this->method == 'GET') {
            return true;
        }
        return false;
    }

    public function filter($data)
    {
        return trim(strip_tags($data));
    }

    public function requestData()
    {
       return $this->requestData;
    }

}