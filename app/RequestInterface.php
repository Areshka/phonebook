<?php

interface RequestInterface {

    public function isPost();

    public function isGet();

    public function filter($data);

    public function requestData();

}