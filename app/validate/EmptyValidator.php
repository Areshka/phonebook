<?php
class EmptyValidator extends ValidatorAbstract
{
    protected $requestData;
    public $errorEmpty;

    public function __construct()
    {
        $this->requestData = $_REQUEST;
    }

    public function validate($data)
    {
        if (empty($data)) {
            $this->errorEmpty = " Write in this field";
        }
        return $this->errorEmpty;
    }

}
