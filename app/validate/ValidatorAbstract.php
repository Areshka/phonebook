<?php

abstract class ValidatorAbstract
{
    abstract public function validate($data);

}