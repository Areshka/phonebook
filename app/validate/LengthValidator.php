<?php

class LengthValidator extends ValidatorAbstract
{

    protected $requestData;

    public function __construct()
    {
        $this->requestData = $_REQUEST;
    }

    public function validate($data)
    {
        switch ($data) {
            case $this->requestData['phone']:
                if (strlen($this->requestData['phone']) > 15) {
                    $error_length = "Field phone must contain no more than 15 characters";
                }
                break;
            case $this->requestData['name']:
                if (strlen($this->requestData['name']) > 20) {
                    $error_length = "Field name must contain no more than 20 characters";
                }
                break;
            case $this->requestData['surname']:
                if (strlen($this->requestData['surname']) > 30) {
                    $error_length = "Field surname must contain no more than 30 characters";
                }
                break;
            case $this->requestData['city']:
                if (strlen($this->requestData['city']) > 30) {
                    $error_length = "Field city must contain no more than 30 characters";
                }
                break;
            case $this->requestData['street']:
                if (strlen($this->requestData['street']) > 30) {
                    $error_length = "Field street must contain no more than 30 characters";
                }
                break;
            case $this->requestData['house']:
                if (strlen($this->requestData['house']) > 10) {
                    $error_length = "Field house must contain no more than 10 characters";
                }
                break;
            case $this->requestData['flat']:
                if (strlen($this->requestData['flat']) > 10) {
                    $error_length = "Field flat must contain no more than 10 characters";
                }
                break;

        }
        return $error_length;
    }

}
